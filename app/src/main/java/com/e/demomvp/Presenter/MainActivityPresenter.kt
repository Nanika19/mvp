package com.e.demomvp.Presenter

import com.e.demomvp.Contract.ContractInterface
import com.e.demomvp.Model.MainActivityModel
import com.e.demomvp.View.MainActivity

class MainActivityPresenter(var _view: MainActivity):ContractInterface.Presenter {

    var model:ContractInterface.Model=MainActivityModel()
    override fun incrementValue() {

        model.incrementCounter()
      //  _view.updateViewData()

    }

    override fun getCounter(): String {
        return  model.getCounter().toString()

    }
}