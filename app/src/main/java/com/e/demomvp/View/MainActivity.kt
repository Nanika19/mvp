package com.e.demomvp.View

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.e.demomvp.*
import com.e.demomvp.Contract.ContractInterface
import com.e.demomvp.Contract.LoginInterface
import com.e.demomvp.Presenter.LoginActivityPresenter
import com.e.demomvp.Presenter.MainActivityPresenter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), LoginInterface.LoginView {


    var loginPresenter: LoginInterface.LoginPresenter? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
      loginPresenter=LoginActivityPresenter(this)
        (loginPresenter as LoginActivityPresenter).getData(et_login_username.text.toString(),et_login_password.text.toString())



    }

    override fun sucessmsg() {
        ShowToast( "LoginSucessFull")
    }

    override fun errormsg() {
        ShowToast( "Login Failed")

    }


/*
    override fun initView() {

        tv_value.text=presenter?.getCounter()

    }

    override fun updateViewData() {
        tv_value.text=presenter?.getCounter()

    }*/
}