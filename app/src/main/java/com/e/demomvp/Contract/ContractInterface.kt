package com.e.demomvp.Contract

interface ContractInterface{

    interface View{
       fun initView()
        fun updateViewData()
    }

    interface Model{

        fun getCounter():Int
        fun incrementCounter()

    }
    interface Presenter{

        fun incrementValue()
        fun getCounter():String

    }
}