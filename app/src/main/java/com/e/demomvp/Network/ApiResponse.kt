package com.e.demomvp.Network

import retrofit2.Response

interface ApiResponse {
    fun onResponse(response: Response<*>?, key: String?)
    fun onError(t: Throwable?, key: String?)
}