import com.e.demomvp.CommonUtils
import com.e.demomvp.Network.GetServices
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {
    private var retrofit: Retrofit? = null
    fun getClient(): GetServices {
        if (retrofit == null)

            retrofit = getRetrofit()

        return retrofit!!.create(GetServices::class.java)
    }

    private fun getRetrofit(): Retrofit? {

        var interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        var client = OkHttpClient.Builder().addInterceptor(interceptor).build()

        var create = GsonConverterFactory.create()
        val retrofit = Retrofit.Builder().addConverterFactory(create).client(client).baseUrl(
            CommonUtils.baseUrl
        ).build()

        return retrofit
    }

}
fun apiHitter()= RetrofitInstance.getClient()