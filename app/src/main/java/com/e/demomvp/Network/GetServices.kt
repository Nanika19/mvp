package com.e.demomvp.Network

import com.google.gson.JsonElement
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface GetServices {

    @POST(" lk")
    @FormUrlEncoded

    fun loginAccount(
        @Field("Password") pasword: String,
        @Field("name") name: String
    ): Call<Data>

    @POST(" lkjhed")
    @FormUrlEncoded

    fun signUpAccount(
        @Field("name") name: String
    ): Call<JsonElement>
}