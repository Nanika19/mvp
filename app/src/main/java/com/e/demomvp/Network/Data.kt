package com.e.demomvp.Network

import com.google.gson.annotations.SerializedName

data class Data (@SerializedName("id_post") val id_post : Int,
                  @SerializedName("title") val title : String,
                  @SerializedName("logo") val logo : String,
                  @SerializedName("description") val description : String,
                  @SerializedName("like_status") val like_status : String,
                  @SerializedName("like_count") val like_count : Int,
                  @SerializedName("comments") val comments : List<String>
)