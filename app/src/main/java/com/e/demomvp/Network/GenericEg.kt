package com.e.demomvp.Network

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonElement
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenericEg<T> {

    fun getresponse(call: Call<T>, onResponse: (T?) -> Unit, onError:(String)->Unit) {

        call.enqueue(object : Callback<T> {

            override fun onFailure(call: Call<T?>, t: Throwable) {
                onError.invoke( t.localizedMessage)
            }

            override fun onResponse(call: Call<T?>, response: Response<T?>) {
                onResponse.invoke(response.body())
            }
        })
    }


}
/*
object APIResponse {
    private val TAG = APIResponse::class.java!!.getSimpleName()
    fun <T> callRetrofit(call: Call<T>, strApiName:String, context: Context, apiListener:ApiListener) {

        call.enqueue(object: Callback<T>() {
        override    fun onResponse(call:Call<T>, response: Response<T>) {
                if (strApiName.equals("LoginApi", ignoreCase = true))
                {
                    if (response.isSuccessful())
                    {
                        Log.d(TAG, "onResponse: " + response.body().toString())

                    }
                    else
                    {
                        try
                        {
                            Log.d(TAG, "onResponse: " + response.errorBody().string())
                            apiListener.error(strApiName, response.errorBody().string())
                            progressDialog.dismiss()
                        }
                        catch (e:IOException) {
                            e.printStackTrace()
                        }
                    }
                }
                else if (strApiName.equals("", ignoreCase = true))
                {
                    //Patient user = (Patient) response.body();
                }
            }
        override    fun onFailure(call:Call<T>, t:Throwable) {
                Log.d(TAG, "onFailure: " + t.toString())
                if (strApiName.equals("searchNearbyTest", ignoreCase = true))
                {
                    apiListener.failure(strApiName, t.toString())
                }

            }
        })
    }
}*/
