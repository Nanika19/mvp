package com.e.demomvp.Network

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiService<T> {
    operator fun get(apiResponse: ApiResponse, methodName: Call<T>, key: String?) {
        methodName.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                apiResponse.onResponse(response, key)
            }
            override fun onFailure(call: Call<T>, t: Throwable) {
                apiResponse.onError(t, key)
            }
        })
    }
}